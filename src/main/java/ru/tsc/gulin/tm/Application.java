package ru.tsc.gulin.tm;

import ru.tsc.gulin.tm.api.ICommandRepository;
import ru.tsc.gulin.tm.constant.ArgumentConst;

import ru.tsc.gulin.tm.constant.TerminalConst;
import ru.tsc.gulin.tm.model.Command;
import ru.tsc.gulin.tm.repository.CommandRepository;
import ru.tsc.gulin.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (processArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void close() {
        System.exit(0);
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (isMemoryLimit ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    public static void showErrorArgument(String arg) {
        System.out.printf("Error! This argument `%s` not supported... \n", arg);
    }

    public static void showErrorCommand(String arg) {
        System.out.printf("Error! This command `%s` not supported... \n", arg);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Yuri Gulin");
        System.out.println("E-mail: ygulin@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("1.2.0");
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
