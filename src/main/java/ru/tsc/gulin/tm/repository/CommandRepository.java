package ru.tsc.gulin.tm.repository;

import ru.tsc.gulin.tm.api.ICommandRepository;
import ru.tsc.gulin.tm.constant.ArgumentConst;
import ru.tsc.gulin.tm.constant.TerminalConst;
import ru.tsc.gulin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Show system info."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Show application version info."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Show terminal commands."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show argument list."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show command list."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO,
            ABOUT,
            VERSION,
            HELP,
            ARGUMENTS,
            COMMANDS,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
